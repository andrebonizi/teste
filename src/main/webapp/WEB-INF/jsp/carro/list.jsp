<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" />

<!-- colocar no final desta página -->


<h1>Carro (List)</h1>

<table class="table table-striped">
    <tr>
        <th>ID</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Ações</th>
    </tr>
    <c:forEach items="${carroList}" var="car">
        <tr>
            <td>${car.id}</td>
            <td>${car.marca}</td>
            <td>${car.modelo}</td>
            <td></td>
        </tr>
    </c:forEach>
</table>

${mensagem} <br/> ${data}
<jsp:include  page="/WEB-INF/jsp/inc/rodape.jsp" />
