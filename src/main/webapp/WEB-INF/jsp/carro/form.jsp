<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carro</title>
    </head>
    <body>
        <h1>Carro (Cadastro)</h1>

        <c:if test="${not empty errors}">
            <ul>
                <c:forEach items="${errors}" var="err">
                    <li>${err.message}</li>
                </c:forEach>
            </ul>
        </c:if>

        <form action="${linkTo[CarroController].save}" method="POST">
            <div>
                <label>Marca</label>                          
                <input type="text" name="carro.marca" />
            </div>

            <div>
                <label>Modelo</label>
                <input type="text" name="carro.modelo" />
            </div>

            <div>
                <button type="submit">Gravar</button>
            </div>

        </form>
    </body>
</html>
