<%-- 
    Document   : form
    Created on : 26/09/2018, 20:08:17
    Author     : ANDRE
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New People Page</title>
    </head>
    <body>    
        <h1>Join our people!</h1>
        <form class="form container" style="align-items: center;" action="${linkTo[PessoaController].save}" method="POST">
            <div class="row">
                <label class="col-2">Nome</label>                          
                <input class="col-2" type="text" name="nome" />
            </div>
            <div class="row">
                <label class="col-2">Login</label>                          
                <input class="col-2" type="text" name="login" />
            </div>
            <div class="row">
                <label class="col-2">Senha</label>                          
                <input class="col-2" type="text" name="senha" />
            </div>
            <div>
                <button  class="btn btn-outline-success" type="submit">Cadastrar</button>
            </div>
        </form>
    </body>
</html>
<c:import url="/WEB-INF/jsp/inc/rodape.jsp" /> 
