<%-- 
    Document   : catCadastro
    Created on : 24/09/2018, 00:45:07
    Author     : Alexandre
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" /> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Category</title>
    </head>
    <body>
        <h1>New Category</h1>

        <c:if test="${not empty errors}">
            <ul>
                <c:forEach items="${errors}" var="err">
                    <li>${err.message}</li>
                </c:forEach>
            </ul>
        </c:if>
        
        <form class="container" action="${linkTo[CategoryController].save}" method="POST">
            <div>
                <label>Nome</label>
                <input type="text" name="Name" value="${Name}"/>
            </div>
	    
            <div>
                <button  class="btn btn-outline-success" type="submit">Gravar</button>
            </div>
        </form>
    </body>
</html>
<c:import url="/WEB-INF/jsp/inc/rodape.jsp" /> 