<%-- 
    Document   : listCat
    Created on : 24/09/2018, 02:30:20
    Author     : Alexandre
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" />

<!-- colocar no final desta página -->


<h1>Category List</h1>
<div class="container">
<table class="table table-striped">
    <tr>
        <th>Nome</th>
    </tr>
    <c:forEach items="${categoryList}" var="cat">
        <tr>
            <td>${cat.name}</td>
        </tr>
    </c:forEach>
</table>
</div>
<div style="text-align: center">
            <a class="btn btn-success " href="categoria/new"> Adicionar Categoria </a>
        </div>
<jsp:include  page="/WEB-INF/jsp/inc/rodape.jsp" />


