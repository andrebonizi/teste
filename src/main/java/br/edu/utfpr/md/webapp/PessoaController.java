/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.edu.utfpr.md.converter.ObjectIdVRaptorConverter;
import br.edu.utfpr.md.webapp.dao.CategoryDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaDocumentDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaPessoaDAO;
import br.edu.utfpr.md.webapp.model.Pessoa;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.bson.types.ObjectId;

/**
 *
 * @author Alexandre
 */
@Controller
@Path("/pessoa")
public class PessoaController {
    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Inject
    private CategoryDAO categoryDAO;
    
    @Inject
    private MorphiaPessoaDAO mPessoaDAO;
    @Inject
    private MorphiaDocumentDAO mDocumentDAO;
    
    public PessoaController() {
    }
    
    @Path("/new")
    @Get
    public void form() {
    }
    @Path(value = {"", "/"})
    @Get
    public List<Pessoa> list() {
        return mPessoaDAO.find().asList();
    }
    
    @Path("delete/{id}")
    public void delete(ObjectId pessoa){
        System.out.println(pessoa+" foi");
        try {
            //this.carroDAO.insert(carro);
            this.mPessoaDAO.deleteById(pessoa);
        }catch(Exception ex) {
            ex.printStackTrace();
            validator.add(new SimpleMessage("dao", "Erro ao deletar Pessoa"));
        }
        result.redirectTo(this).list();
    }
    
    @Post
    public void save(@Valid String nome, String login, String senha) {
        validator.onErrorForwardTo(this).form();
        Pessoa pessoa = new Pessoa(nome,login,senha);
        
        try {
            //this.carroDAO.insert(carro);
            this.mPessoaDAO.save(pessoa);
        }catch(Exception ex) {
            ex.printStackTrace();
            validator.add(new SimpleMessage("dao", "Erro ao gravar Pessoa"));
        }
        //Cadastrar no banco de dados....
        //Redirecionamento
        result.redirectTo(this).list();
    }
    
    
}
