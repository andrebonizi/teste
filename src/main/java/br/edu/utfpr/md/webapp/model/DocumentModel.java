/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.model;

import java.util.Date;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 *
 * @author André Bonizi
 */
@Entity
public class DocumentModel {
    @Id
    private ObjectId id;
    
    //@NotNull(message = "Descricao não pode ser nulo")
    @Size(min=10, max=50) private String Descricao;
    
    //@NotNull(message = "fileName não pode ser nulo")
    @Size(min=3, max=50) private String fileName;
    
    //@NotNull(message = "LastUpdate não pode ser nulo")
    private Date LastUpdate;
    
    //@NotNull(message = "Date não pode ser nulo")
    private Date date;
    
    //@NotNull (message = "Categoria n pode ser nulo")
    private Category categoria;

    public Category getCategoria() {
        return categoria;
    }

    public void setCategoria(Category categoria) {
        this.categoria = categoria;
    }
    
    private String Keyword;

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String Keyword) {
        this.Keyword = Keyword;
    }
    public DocumentModel(){    
    }

    public DocumentModel(String Descricao, String fileName, Date LastUpdate, Date date, String keyword) {
        this.Descricao = Descricao;
        this.fileName = fileName;
        this.LastUpdate = LastUpdate;
        this.date = date;
        this.Keyword = keyword;
    }

    public ObjectId getId() {
        return id;
    }

    public String getDescricao() {
        return Descricao;
    }

    public String getFileName() {
        return fileName;
    }

    public Date getLastUpdate() {
        return LastUpdate;
    }

    public Date getDate() {
        return date;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setLastUpdate(Date LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    public void setDate(Date date) {
        this.date = date;
    }    
}
