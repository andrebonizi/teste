/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;

import br.edu.utfpr.md.webapp.model.DocumentModel;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 *
 * @author Alexandre
 */
public class DocumentModelDAO extends GenericDAO<DocumentModel, ObjectId> {

    @Inject
    private MongoDatabase db;

    private MongoCollection doccCollection;

    @PostConstruct
    public void init() {
        this.doccCollection = db.getCollection("Document");
    }

    @Override
    public void insert(DocumentModel obj) {
        Document docc = new Document()
                .append("descricao", obj.getDescricao())
                .append("fileName", obj.getFileName())
                .append("date", obj.getDate())
                .append("lastUpdate", obj.getLastUpdate())
                .append("keywords", obj.getKeyword());

        doccCollection.insertOne(docc);
    }

    @Override
    public void update(DocumentModel obj) {
        Document docc = new Document()
                .append("descricao", obj.getDescricao())
                .append("fileName", obj.getFileName())
                .append("date", obj.getDate())
                .append("lastUpdate", obj.getLastUpdate())
                .append("keywords", obj.getKeyword());
        
        Document docc2 = new Document()
                .append("id", obj.getId());
        
        doccCollection.updateOne(docc2, docc);
    }

    @Override
    public void delete(DocumentModel obj) {
        Document docc = new Document()
                .append("descricao", obj.getDescricao())
                .append("fileName", obj.getFileName())
                .append("date", obj.getDate())
                .append("lastUpdate", obj.getLastUpdate())
                .append("keywords", obj.getKeyword())
                .append("categoria", obj.getCategoria())
                .append("id", obj.getId());
        
        doccCollection.deleteOne(docc);
    }

    @Override
    public List<DocumentModel> getAll() {
        List<DocumentModel> ls = new ArrayList<>();

        MongoCursor cursor = doccCollection.find().iterator();
        while (cursor.hasNext()) {
            Document doc = (Document) cursor.next();
            DocumentModel docm = new DocumentModel(doc.getString("fileName"), doc.getString("Descricao"), doc.getDate("date"), doc.getDate("lastUpdate"), doc.getString("keywords"));
            docm.setId(doc.getObjectId("_id"));
            ls.add(docm);
        }

        return ls;
    }

    @Override
    public DocumentModel getById(ObjectId id) {
        DocumentModel docModel;
        Document docc = new Document().append("id", id);
        
        //Aqui pode ter problema ao fazer o cast de uma ArrayList para um objeto
        docModel = (DocumentModel) doccCollection.find(docc);
        
        return docModel;
    }

}
