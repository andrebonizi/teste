/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;



import br.edu.utfpr.md.webapp.model.Pessoa;
import com.mongodb.MongoClient;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
/**
 *
 * @author Alexandre
 */
public class MorphiaPessoaDAO extends BasicDAO<Pessoa, ObjectId>{
    @Inject
    public MorphiaPessoaDAO(MongoClient mongoClient) {
        super(Pessoa.class, mongoClient, new Morphia(), MongoClientProvider.DATABASE);
    }   
    public Pessoa getById(ObjectId id){
        
        return this.getDatastore().find(Pessoa.class).field("_id").equal(id).get();
    }
}