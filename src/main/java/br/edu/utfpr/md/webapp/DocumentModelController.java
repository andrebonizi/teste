/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.edu.utfpr.md.webapp.dao.DocumentModelDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaDocumentDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaCategoryDAO;
import br.edu.utfpr.md.webapp.model.Category;
import br.edu.utfpr.md.webapp.dao.CategoryDAO;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import br.edu.utfpr.md.webapp.model.DocumentModel;
import org.bson.Document;
import org.bson.types.ObjectId;


/**
 *
 * @author André Bonizi
 */
@Controller
@Path("/doc")
public class DocumentModelController {    
    @Inject
    private Result result;

    @Inject
    private Validator validator;

    
    @Inject
    private MorphiaDocumentDAO mDocumentDAO;
    
    @Inject
    private MorphiaCategoryDAO mCategoryDAO;
    
    @Inject
    private CategoryDAO categoryDAO;
    
    public DocumentModelController() {
    }

    @Path("/new")
    @Get
    public List<Category> form() {
        return mCategoryDAO.find().asList();
    }

    @Path(value = {"", "/"})
    @Get
    public List<DocumentModel> list() {
        
        return mDocumentDAO.find().asList();
    }
    
   
    
    @Post
    public void save(@Valid DocumentModel document, String aux) {
        validator.onErrorForwardTo(this).form();
        
        document.setCategoria(mCategoryDAO.getById(aux));
        
        try {
            //this.carroDAO.insert(carro);
            this.mDocumentDAO.save(document);
        }catch(Exception ex) {
            ex.printStackTrace();
            validator.add(new SimpleMessage("dao", "Erro ao gravar Documento"));
        }
        //Cadastrar no banco de dados....
        //Redirecionamento
        result.redirectTo(this).list();
    }
}
