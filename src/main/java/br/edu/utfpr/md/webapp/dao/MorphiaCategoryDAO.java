/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;
import br.edu.utfpr.md.webapp.model.Category;
import com.mongodb.MongoClient;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
/**
 *
 * @author Alexandre
 */
public class MorphiaCategoryDAO extends BasicDAO<Category, ObjectId>{
    @Inject
    public MorphiaCategoryDAO(MongoClient mongoClient) {
        super(Category.class, mongoClient, new Morphia(), MongoClientProvider.DATABASE);
    }   
    public Category getById(String id){
        
        Query<Category> query = createQuery().field("id").equal(new ObjectId(id));
        return this.findOne(query);
    }
}
