/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;
import br.edu.utfpr.md.webapp.model.DocumentModel;
import com.mongodb.MongoClient;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;

/**
 *
 * @author Alexandre
 */
public class MorphiaDocumentDAO extends BasicDAO<DocumentModel, ObjectId>{
    @Inject
    public MorphiaDocumentDAO(MongoClient mongoClient) {
        super(DocumentModel.class, mongoClient, new Morphia(), MongoClientProvider.DATABASE);
    }   

  
}
