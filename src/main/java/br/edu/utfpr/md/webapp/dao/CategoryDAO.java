/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;

import br.edu.utfpr.md.webapp.model.Category;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 *
 * @author Alexandre
 */
@RequestScoped
public class CategoryDAO extends GenericDAO<Category, ObjectId>{
    @Inject
    private MongoDatabase db;
    
    private MongoCollection catCollection;
    
     @PostConstruct
    public void init() {
        this.catCollection = db.getCollection("Category");
    }
    
     @Override
    public void update(Category obj) {
        Document docc = new Document()
                .append("Name", obj.getName());
        
        Document docc2 = new Document()
                .append("id", obj.getId());
        catCollection.updateOne(docc2, docc);
    }

    @Override
    public void delete(Category obj) {
        Document docc = new Document()
                .append("Name", obj.getName())
                .append("id", obj.getId());
        catCollection.deleteOne(docc);
    }    
    
        @Override
    public List<Category> getAll() {
        List<Category> ls = new ArrayList<>();
        
        MongoCursor cursor;
        cursor = catCollection.find().iterator();
        while (cursor.hasNext()) {
            Document doc = (Document) cursor.next();
            Category cat = new Category(doc.getString("Name"));
            cat.setId(doc.getObjectId("_id"));
            ls.add(cat);
        }        
        return ls;
    }
    
    public Category getById(String id){
        Category docCategory;
        Document docc = new Document().append("id", id);
        
        //Aqui pode ter problema ao fazer o cast de uma ArrayList para um objeto
        docCategory = (Category) catCollection.find(docc);
        
        return docCategory;
    }

    @Override
    public void insert(Category obj) {
        Document cat = new Document()
                .append("Name", obj.getName());
        catCollection.insertOne(cat);
    }

    @Override
    public Category getById(ObjectId id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
