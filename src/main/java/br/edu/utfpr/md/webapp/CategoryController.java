package br.edu.utfpr.md.webapp;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.edu.utfpr.md.webapp.dao.CategoryDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaCategoryDAO;
import br.edu.utfpr.md.webapp.model.Category;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;


/**
 *
 * @author Alexandre
 */
@Controller
@Path("/categoria")
public class CategoryController {    
    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Inject
    private CategoryDAO categoryDAO;
    
    @Inject
    private MorphiaCategoryDAO mCategoryDAO;
    
    public CategoryController() {
    }
    

    @Path("/new")
    @Get
    public void form() {
    }

    @Path(value = {"", "/"})
    @Get
    public List<Category> list() {
        return mCategoryDAO.find().asList();
    }
    
    
    @Post
    public void save(@Valid String Name) {
        validator.onErrorForwardTo(this).form();
        Category c = new Category(Name);
        try {
            this.mCategoryDAO.save(c);
        }catch(Exception ex) {
            validator.add(new SimpleMessage("dao", "Erro ao gravar Categoria"));
        }
        result.redirectTo(this).list();
    }
}
