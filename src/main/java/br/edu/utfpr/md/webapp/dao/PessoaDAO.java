/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.dao;

import br.edu.utfpr.md.webapp.model.Pessoa;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 *
 * @author Alexandre
 */
public class PessoaDAO extends GenericDAO<Pessoa, ObjectId>{
    @Inject
    private MongoDatabase db;
    
    private MongoCollection pesCollection;
    
     @PostConstruct
    public void init() {
        this.pesCollection = db.getCollection("Pessoa");
    }
    
     @Override
    public void update(Pessoa obj)  {
        Document docc = new Document()
                .append("Name", obj.getNome())
                .append("Login", obj.getLogin())
                .append("Senha", obj.getSenha());
        
        Document docc2 = new Document()
                .append("id", obj.getId());
        
        pesCollection.updateOne(docc2, docc);
    }

    @Override
    public void delete(Pessoa obj)  {
        Document docc = new Document()
                .append("Name", obj.getNome())
                .append("login", obj.getLogin())
                .append("senha", obj.getSenha())
                .append("id", obj.getId());
        
        
        pesCollection.deleteOne(docc);
    }   
    
    @Override
    public void insert(Pessoa obj) {
        Document pes;
        pes = new Document()
                .append("name", obj.getNome())
                .append("login", obj.getLogin())
                .append("senha", obj.getSenha());
        pesCollection.insertOne(pes);
    }
    
    @Override
    public List<Pessoa> getAll() {
        List<Pessoa> ls = new ArrayList<>();
        
        MongoCursor cursor = pesCollection.find().iterator();
        while (cursor.hasNext()) {
            Document doc = (Document) cursor.next();
            Pessoa pes = new Pessoa(doc.getString("name"), doc.getString("login"));
            pes.setId(doc.getObjectId("_id"));
            ls.add(pes);
        }
        
        return ls;
    }

    @Override
    public Pessoa getById(ObjectId id) {
        Pessoa docModel;
        Document docc = new Document().append("id", id);
        
        //Aqui pode ter problema ao fazer o cast de uma ArrayList para um objeto
        docModel = (Pessoa) pesCollection.find(docc);
        
        return docModel;
    }
}
