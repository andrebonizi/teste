package br.edu.utfpr.md.webapp.model;

import javax.validation.constraints.NotNull;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class Pessoa {
    @Id
    private ObjectId id;
    
    @NotNull(message = "Nome nao pode ser nulo")
    private String nome;
    @NotNull(message = "Login nao pode ser nulo")
    private String login;
    @NotNull(message = "Senha nao pode ser nulo")
    private String senha;
      
    
    public Pessoa(){
    }
    
    public Pessoa(String id){
        
    }
    
    public Pessoa(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    public Pessoa(String nome, String login, String senha) {
        this.nome = nome;
        this.login = login;
        this.senha = senha;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    
}
