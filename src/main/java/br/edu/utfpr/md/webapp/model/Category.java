/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.model;

import javax.validation.constraints.NotNull;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 *
 * @author André Bonizi
 */
@Entity
public class Category {
    @Id
    private ObjectId id;
    
    @NotNull(message = "Name não pode ser nulo")
    private String Name;

    public Category() {
    }

    public String getName() {
        return Name;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Category(String Name) {
        this.Name = Name;
    }


    
}
