package br.edu.utfpr.md.webapp;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.edu.utfpr.md.webapp.dao.CarroDAO;
import br.edu.utfpr.md.webapp.dao.MorphiaCarroDAO;
import br.edu.utfpr.md.webapp.model.Carro;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;

@Controller
@Path("/car")
public class CarroController {

    @Inject
    private Result result;

    @Inject
    private Validator validator;

    @Inject
    private CarroDAO carroDAO;
    
    @Inject
    private MorphiaCarroDAO mCarroDao;
            

    @Path("/new")
    @Get
    public void form() {
    }

    @Path(value = {"","/"})
    @Get
    public List<Carro> list() {
        result.include("mensagem", "Esta é uma mensagem qualquer");
        result.include("data", new Date());
        
        // Buscando os carros do banco de dados
        //return carroDAO.getAll();
        
        return mCarroDao.find().asList();
    }
    
    
    @Post
    public void save(@Valid Carro carro) {
        validator.onErrorForwardTo(this).form();
        
        try {
            //this.carroDAO.insert(carro);
            this.mCarroDao.save(carro);
        }catch(Exception ex) {
            ex.printStackTrace();
            validator.add(new SimpleMessage("dao", "Erro ao gravar Carro"));
        }
        
        /*        
        if(carro.getMarca() == null || carro.getMarca().isEmpty()) {
            validator.add(new SimpleMessage("carro.marca", "Marca está vazia!"));
        }
        if(carro.getModelo() == null || carro.getModelo().isEmpty()) {
            validator.add(new SimpleMessage("carro.modelo", "Modelo está vazio!"));
        }
         */
        
        

        //Cadastrar no banco de dados....
        //Redirecionamento
        result.redirectTo(this).list();
    }

}
