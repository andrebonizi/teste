/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.md.webapp.model;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 *
 * @author André Bonizi
 */
@Entity
public class Document {
    @Id
    private ObjectId id;
    
    @NotNull(message = "Descricao não pode ser nulo")
    private String Descricao;
    
    @NotNull(message = "fileName não pode ser nulo")
    private String fileName;
    
    @NotNull(message = "LastUpdate não pode ser nulo")
    private Date LastUpdate;
    
    @NotNull(message = "Date não pode ser nulo")
    private Date date;
    public Document(){    
    }

    public Document(String Descricao, String fileName, Date LastUpdate, Date date) {
        this.Descricao = Descricao;
        this.fileName = fileName;
        this.LastUpdate = LastUpdate;
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public String getDescricao() {
        return Descricao;
    }

    public String getFileName() {
        return fileName;
    }

    public Date getLastUpdate() {
        return LastUpdate;
    }

    public Date getDate() {
        return date;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setLastUpdate(Date LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    public void setDate(Date date) {
        this.date = date;
    }    
}
