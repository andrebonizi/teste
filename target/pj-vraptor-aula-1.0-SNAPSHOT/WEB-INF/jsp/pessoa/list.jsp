<%-- 
    Document   : list
    Created on : 26/09/2018, 20:09:02
    Author     : ANDRE
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" />

        
        <h1>${loggedUser.pessoa.nome}</h1>
        <br>
        <h1> People</h1>
        <div class="container">
            <table class="table table-striped ">
                <tr>
                    <th>Nome</th>
                    <th>Login</th>
                    <th>Senha</th>
                    <th>Ações</th>
                        
                </tr>
                <c:forEach items="${pessoaList}" var="pessoa">
                    <tr>
                        <td>${pessoa.nome}</td>
                        <td>${pessoa.login}</td>
                        <td>${pessoa.senha}</td>
                        <td>
                            <a class="btn btn-danger" href="${linkTo[PessoaController].delete}${pessoa.id}"> Excluir</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        
        <a class="btn btn-primary " href="pessoa/new"> Adicionar Pessoa </a>
        
<jsp:include  page="/WEB-INF/jsp/inc/rodape.jsp" />
