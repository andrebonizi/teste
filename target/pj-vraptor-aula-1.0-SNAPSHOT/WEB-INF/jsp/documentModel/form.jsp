

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" /> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Documents</title>
    </head>
    <body>
             <h1>Cadastro</h1>

        <c:if test="${not empty errors}">
            <ul>
                <c:forEach items="${errors}" var="err">
                    <li>${err.message}</li>
                </c:forEach>
            </ul>
        </c:if>

             <form class="form container" style="align-items: center;" action="${linkTo[DocumentModelController].save}" method="POST">
            <div class="row">
                <label class="col-2">Descricao</label>                          
                <input class="col-2" type="text" name="document.descricao" />
            </div>

            <div class="row">
                <label class="col-2">LastUpdate</label>
                <input class="col-2" type="text" name="document.lastUpdate" />
            </div>
	    <div class="row">
                <label class="col-2">fileName</label>
                <input class="col-2" type="text" name="document.fileName" />
            </div>
	    <div class="row">
                <label class="col-2">Date</label>
                <input class="col-2" type="text" name="document.date" />
            </div>
            <div class="dropdown row">
                <label class="col-2">Categoria</label>
                
                <select class="col-2 selectpicker" name="aux">
                    <c:forEach items="${categoryList}" var="cat">
                        <option value="${cat.id}">${cat.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="row">
                <label class="col-2">Keyword</label>
                <input class="col-2" type="text" name="document.keyword" />
            </div>
            <div>
                <button  class="btn btn-outline-success" type="submit">Gravar</button>
            </div>
        </form>
    </body>
</html>
<c:import url="/WEB-INF/jsp/inc/rodape.jsp" /> 