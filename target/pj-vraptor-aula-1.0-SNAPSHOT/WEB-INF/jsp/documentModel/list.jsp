
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<c:import url="/WEB-INF/jsp/inc/cabecalho.jsp" /> 
<html>
    
    <body>
        <h1> Documents</h1>
        <div class="container">
            <table class="table table-striped ">
                <tr>
                    <th>Description</th>
                    <th>Filename</th>
                    <th>Date</th>
                    <th>LastUpdate</th>
                    <th>Category</th>
                    <th>Keyword</th>

                </tr>
                <c:forEach items="${documentModelList}" var="doc">
                    <tr>
                        <td>${doc.descricao}</td>
                        <td>${doc.fileName}</td>
                        <td>${doc.date}</td>
                        <td>${doc.lastUpdate}</td>
                        <td>${doc.categoria}</td>
                        <td>${doc.keyword}</td>
                    </tr>
                </c:forEach>
            </table>
    </div>
        <div style="text-align: center">
            <a class="btn btn-primary " href="doc/new"> Adicionar Documento </a>
            
        </div>
    </body>
</html>

<c:import url="/WEB-INF/jsp/inc/rodape.jsp" /> 
